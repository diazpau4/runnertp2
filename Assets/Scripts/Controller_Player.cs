﻿using UnityEngine;
using UnityEngine.UI;

public class Controller_Player : MonoBehaviour //control de jugador
{
    private Rigidbody rb; //Le da un rigidbody al player
    public float jumpForce = 10; //la fuerza de salto
    public float dashforce = 100;
    public int vida = 100;
    public Text life;
    private float initialSize; //El tamaño de inicio
    private int i = 0; 
    private bool floored; //Variable para detectar si el personaje esta tocando el piso y asi puede saltar 

    public Image barra;
    public float naftaAc = 100;
    public float llena = 100;

    private void Start() //inicializa el RB y la escala inicial del personaje
    {
        rb = GetComponent<Rigidbody>();
        initialSize = rb.transform.localScale.y;
        life.text = vida.ToString();
    }

    void Update() //Actualiza el getinput que llama a Jump y Duck
    {
        GetInput();
        logicatongas();

    }
    void LateUpdate()
    {
        logicatongas();
    }

    private void GetInput() //llama a las funciones Jump y Duck
    {
        Jump();
        Duck();      
    }

    private void Jump() //permite el salto del personaje
    {
        if (floored) //permite saltar si esta tocando el piso
        {
            if (Input.GetKeyDown(KeyCode.W)) //salta con la tecla W
            {
                rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse); //si salta, con el RB calcula la fuerza de salto con el impulso
            }
        }
    }


    private void Duck() //cambia la escala del personaje para que este pueda agacharse
    {
        if (floored)
        {
            if (Input.GetKey(KeyCode.S)) //se agacha con la letra S
            {
                if (i == 0) //si esta en 0 la escala, la transforma
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, rb.transform.localScale.y / 2, rb.transform.localScale.z); //transforma la escala
                    i++;
                }
            }
            else
            {
                if (rb.transform.localScale.y != initialSize)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, initialSize, rb.transform.localScale.z); //vuelve a la escala original
                    i = 0;
                }
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                rb.AddForce(new Vector3(0, -jumpForce, 0), ForceMode.Impulse); // le agrega un impulso hacia abajo.
            }
        }
    }

    public void OnCollisionEnter(Collision collision) //colisiones 
    {
        if (collision.gameObject.CompareTag("Enemy")) //si colisiona con el algo que tenga el tag Enemy, destruye al personaje y sale el texto game over
        {
            recibirdaño();
        }


        if (collision.gameObject.CompareTag("Floor")) //si el personaje colisiona con todo aquello que tenga el tag Floor, detecta que esta tocando el piso
        {
            floored = true; //afirma que el personaje esta tocando el suelo
        }
    }

    private void OnCollisionExit(Collision collision) //colisión de salida
    {
        if (collision.gameObject.CompareTag("Floor")) //se desactiva el suelo para detectar que el personaje esta saltando
        {
            floored = false; //afirma que el personaje NO esta tocando el suelo
        }
    }
    public void recibirdaño()
    {
        vida -= 25;
        life.text = vida.ToString();
        if (vida == 0)
        {
            dead();
        }
    }
    public void logicatongas()
    {

        barra.fillAmount = naftaAc / llena;
        naftaAc -= 4 * Time.deltaTime;
        if (naftaAc <= 0f)
        {
            dead();
        }
    }
    public void dead()
    {
        Destroy(this.gameObject); //destruye al pesonaje
        Controller_Hud.gameOver = true; //llama al controlador del hud para decirle que es game over
    }

}
