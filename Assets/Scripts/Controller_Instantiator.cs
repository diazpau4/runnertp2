﻿using System.Collections.Generic;
using UnityEngine;

public class Controller_Instantiator : MonoBehaviour //Control de instancias
{
    public List<GameObject> enemies; //lista de enemigos
    public GameObject instantiatePos; //inicializa la posición 
    public float respawningTimer; //respawn con timer
    private float time = 0; //timer

    void Start() //inicializa 
    {
        Controller_Enemy.enemyVelocity = 2; //controla la velocidad de los enemigos
    }

    void Update() //actualiza
    {
        SpawnEnemies(); //respawnea enemigos
        ChangeVelocity(); //les cambia la velocidad
    }

    private void ChangeVelocity() //cambia la velociudad
    {
        time += Time.deltaTime; //hace que el tiempo se vaya sumando 1 vez por segundo x frame
        Controller_Enemy.enemyVelocity = Mathf.SmoothStep(1f, 15f, time / 45f); //control de velocidad de los enemigos
    }

    private void SpawnEnemies() //spawn de enemigos
    {
        respawningTimer -= Time.deltaTime; //respawnea en base al tiempo

        if (respawningTimer <= 0) //timer para saber cuando respawnear
        {
            Instantiate(enemies[UnityEngine.Random.Range(0, enemies.Count)], instantiatePos.transform); //crea enemigos y les pasa una posicion 
            respawningTimer = UnityEngine.Random.Range(2, 6); //timer de respawn
        }
    }


}
