﻿using UnityEngine;
using UnityEngine.UI;

public class Controller_Hud : MonoBehaviour //controla el HUD
{
    public static bool gameOver = false; //setea el gameOver para poder decir si se perdió o no el juego y que así salga o no el cartel de game over.
    public Text distanceText; //setea la distancia del texto    
    public Text gameOverText; //crea el texto gameover
    public Text highScoreText;

    private float distance = 0; //inicializa la distancia en 0
    int valordistancia; //crea un entero llamado valordistancia para despues instanciarla
    int highscore;


    void Start() //incializa
    {
        highscore = SaveHighScore.instance.LoadGame().score;
        valordistancia = (int)distance; //instancia la distancia
        gameOver = false; //setea el gameOver en falso, para que no salga el cartel apenas comienza el juego y no parezca que se perdió la partida sin haber comenzado
        distance = 0; //inicializa la distancia en 0
        distanceText.text = valordistancia.ToString(); //enseña el texto de distancia
        gameOverText.gameObject.SetActive(false); //setea el texto gameOver en falso, para que no salga el cartel apenas comienza el juego 
        highScoreText.text = highscore.ToString();
    }

    void Update() //actualiza
    {

        valordistancia = (int)distance; //actualiza el valor de la distancia dependiendo la distancia recorrida
        if (gameOver) //si se pierde, ocurre lo siguiente
        {
            Time.timeScale = 0; //para el tiempo. es decir los fotogramas
            gameOverText.text = "Game Over \n Total Distance: " + valordistancia.ToString(); //enseña el cartel de game over y la distancia recorrida
            gameOverText.gameObject.SetActive(true); //vuelve positivo el booleano del texto de game over para que aparezca en pantalla

            if(valordistancia> highscore)
            {
                GameData saveData = new GameData();
                saveData.AddScore(valordistancia);
                SaveHighScore.instance.SaveGame(saveData);
            }
            

        }
        else
        {
            distance += Time.deltaTime; //mide la distancia
            distanceText.text = valordistancia.ToString(); //enseña el texto de distancia
        }
    }
    
}

