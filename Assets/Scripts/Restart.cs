﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour //Reinciia el juego
{
    void Update() //Actualiza
    {
        GetInput(); //llama a la función GetInput
    }

    private void GetInput()
    {
        if (Input.GetKeyDown(KeyCode.R)) //Apretando la letra R el juego se reinicia.
        {
            Time.timeScale = 1;  //hace que el tiempo deje de estar detenido
            SceneManager.LoadScene(0); //Carga la escena 0
        }
    }
}
 