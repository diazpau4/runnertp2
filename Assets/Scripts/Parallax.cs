﻿using UnityEngine;

public class Parallax : MonoBehaviour //Control de efecto parallax
{
    public GameObject cam; //Setea la camara
    private float length, startPos; //setea el largo y la posición inicial
    public float parallaxEffect; //setea el efecto parallax
    
    void Start()
    {
        startPos = transform.position.x; //Asigna la posición de comienzo
        length = GetComponent<SpriteRenderer>().bounds.size.x; //Asigna el largo 
    }

    void Update() //actualiza y setea la posición del efecto parallax
    {
        transform.position = new Vector3(transform.position.x - parallaxEffect, transform.position.y, transform.position.z);
        if (transform.localPosition.x < -20)
        {
            transform.localPosition = new Vector3(20, transform.localPosition.y, transform.localPosition.z);
        }
        if (Time.timeScale == 0)
        {
            parallaxEffect = 0;
        }
        

    }
}
