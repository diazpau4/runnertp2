﻿using UnityEngine;

public class Controller_Enemy : MonoBehaviour //Control de enemigos
{
    public static float enemyVelocity; //crea una variable de la velocidad de enemigos  
    private Rigidbody rb; //crea una variable de tipo RB

    void Start() //inicializa
    {
        rb = GetComponent<Rigidbody>(); //asigna la variable Rigidbody a RB
    }

    void Update() //actualiza
    {
        rb.AddForce(new Vector3(-enemyVelocity, 0, 0), ForceMode.Force); //relaciona el rb con la velocidad de los enemigos
        OutOfBounds(); //llama a la función OutOfBounds
    }

    public void OutOfBounds() //setea que cuando el enemigo esta fuera de un cierto limite lo destruye
    {
        if (this.transform.position.x <= -15) //setea la pos limite
        {
            Destroy(this.gameObject); //lo destruye
        }
    }

    public void OnCollisionEnter(Collision collision) //colisiones 
    {
        if (collision.gameObject.CompareTag("Player")) //Si el enemigo colisiona con el objeto con tag player, se destruye
        {
            Destroy(this.gameObject); //destruye el enemigo
            //Controller_Hud.gameOver = true; //llama al controlador del hud para decirle que es game over


        }
    }
}


